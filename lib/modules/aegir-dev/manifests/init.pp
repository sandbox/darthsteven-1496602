# A Puppet module to add some Aegir-dev-specific config

class aegir-dev {
  notice("\n
          Setting up this aegir-up instance for developing Aegir")
  

  
  # Change the hostmaster and provision directories to be server from the host.
  
  include aegir-up
  
  file { "/var/aegir/.drush/provision":
    ensure => link,
    target => "/vagrant/aegir/provision",
    force => true,
    require => Class["aegir::manual_build::backend"],
  }
  
  file { "/var/aegir/hostmaster-6.x-1.x/profiles/hostmaster":
    ensure => link,
    target => "/vagrant/aegir/hostmaster",
    force => true,
    require => Class["aegir::manual_build::frontend"],
  }  

}
