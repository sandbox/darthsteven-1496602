Aegir-up
========

Aegir-up deploys a local instance of the Aegir Hosting System atop Vagrant and
Virtualbox, for development and testing purposes.

**N.B.** Aegir-up is *NOT* intended for production hosting. For a fully managed
production-grade Aegir server check out Koumbit Networks' AegirVPS services
(http://www.koumbit.org/en/services/AegirVPS), or other Aegir Service Providers
(http://community.aegirproject.org/service-providers).


Quick start
-----------

Assuming all the dependencies are installed, you can get started by:

    $ ./aegir-up-init my_project
    ...
    $ cd projects/my_project
    $ vagrant up


Installation
------------

See http://community.openatria.com/team/aegir-up/installation


Usage
-----

See http://community.openatria.com/team/aegir-up/usage


Other Documentaion
------------------

See http://community.openatria.com/team/aegir-up

